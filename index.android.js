import { AppRegistry } from 'react-native';
import App from './app';

AppRegistry.registerComponent('eventyrAndroid', () => App);
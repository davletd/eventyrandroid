import React, { Component } from 'react';
import { ListView } from 'react-native';

import Meteor, { MeteorListView, createContainer } from 'react-native-meteor';
import EventsList from './Components/EventsList.js';
import EventDetails from './Components/EventDetails.js';
import { Scene, Router } from 'react-native-router-flux';

const SERVER_URL = 'ws://eventenix.com/websocket';

class App extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: this.props.events
        };
    }

    componentWillMount() {
        Meteor.connect(SERVER_URL);  
    }

    render() {
        return (
            <Router>
                <Scene key="root" hideNavBar={true}>
                    <Scene key="list" component={EventsList} initial={true} />
                    <Scene key="event" component={EventDetails} />
                </Scene>
            </Router>
        );
    }
}

export default createContainer(() => {
    Meteor.subscribe('events');
    return {
        events: Meteor.collection('events').find({start_time: {$gt: new Date()}})
    };
}, App);

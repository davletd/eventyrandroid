import React, { Component } from 'react';
import { StyleSheet, Text, View, ListView, Image, TouchableHighlight } from 'react-native';
import Meteor, { MeteorListView, createContainer } from 'react-native-meteor';
import { Actions } from 'react-native-router-flux';
const moment = require('moment');

export default class EventsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSource: this.props.events
        };
    }

    renderRow(event) {
        return (
            <TouchableHighlight onPress={() => Actions.event(event)}>
                <View style={styles.eventContainer} >
                    <Text style={styles.eventName}>{moment(event.start_time).calendar()}</Text>
                    <Text style={styles.eventName}>{event.name}</Text>
                    <Image source={{ uri: event.cover.source}} style={styles.canvas}>    
                    </Image>                
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style={styles.container}>
            <MeteorListView
                    collection="events"
                    selector={{$or: [{start_time: {$gte: new Date()}}, {end_time: {$gte: new Date()}}]}}
                    options={{sort: {start_time: 1}}}
                    renderRow={this.renderRow}
                    />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: 'ghostwhite',
    },
    eventContainer: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#333333',
        margin: 15,
        borderColor: 'lightgrey',
        borderStyle: 'solid',
        borderWidth: 1
    },
    eventName: {
        fontFamily: 'sans-serif', 
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
        margin: 10,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    thumbnail:{
        height: 40,
        width: 40,
    },
    canvas: {
        flex:1,
        height: 120,
    }
});

import React, { Component } from 'react';
import { StyleSheet, Text, View, ListView, Image } from 'react-native';

export default class EventDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>{this.props.name}</Text>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    thumbnail:{
        height: 40,
        width: 40,
    },
    canvas: {
        flex:1,
        height: 80
    }
});
